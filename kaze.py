import cv2
import numpy as np
img1=cv2.imread('battery.png')
img2=cv2.imread('scene.png')
MIN_MATCH_COUNT = 6
#surf=cv2.SURF(50000)# 50000 is threshold
kaze = cv2.KAZE_create(40,2)
#surf.hessianThreshold=50000
kp3,des3=kaze.detectAndCompute(img1,None)
kp4,des4=kaze.detectAndCompute(img2,None)
des3=np.float32(des3)
des4=np.float32(des4)
print des3,'--------------------',des4,'----------'
print type(des3),type(des4)
#print len(des3),len(des4)
#img=cv2.drawKeypoints(img1,kp,None,(255,0,0),4)
#cv2.imshow('xcdfv',img2)
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks = 50)
#bf=cv2.BFMatcher(cv2.NORM_HAMMING,crossCheck=True)
#matches=bf.match(des3,des4)
#matches=sorted(matches,key=lambda val: val.distance)
#img3=cv2.drawMatches(img1,kp3,img2,kp4,matches,None,flags=2)
flann = cv2.FlannBasedMatcher(index_params, search_params)

matches = flann.knnMatch(des3,des4,k=2)

good = []
i=0
for m,n in matches:
    if m.distance <0.9*n.distance:
        good.append(m)
    i=i+1
img3=cv2.drawMatches(img1,kp3,img2,kp4,good,None,flags=2)
print good,i
#if len(good)>MIN_MATCH_COUNT:
 #   src_pts = np.float32([ kp3[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
 #  dst_pts = np.float32([ kp4[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
	
 #   M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
 #   matchesMask = mask.ravel().tolist()
	
 #   h,w,p = img1.shape
 #   pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
 #   dst = cv2.perspectiveTransform(pts,M)
	
  #  img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)
  #  print np.int32(dst)

#else:
#    print "Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT)
#    matchesMask = None
#	#print src_pts,'------',dst_pts,''
#draw_params = dict(matchColor = (0,255,0), # draw matches in green color
#                   singlePointColor = None,
#                   matchesMask = matchesMask, # draw only inliers
#                   flags = 2)

#img3 = cv2.drawMatches(img1,kp3,img2,kp4,good,None,**draw_params)
cv2.imshow('kachra',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()